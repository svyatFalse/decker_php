FROM php:7.3.7-fpm

RUN ln -sf /usr/share/zoneinfo/Europe/Samara /etc/localtime

RUN apt-get update \
&& apt-get install -y git \
                      cron \
                      zlib1g-dev \
                      libfreetype6-dev \
                      libzip-dev \
                      libpng-dev \
&& docker-php-ext-configure gd \
        --with-gd \
        --with-freetype-dir=/usr/include/ \
        --with-png-dir=/usr/include/ \
&& docker-php-ext-install pdo pdo_mysql mysqli zip bcmath gd \
&& curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer

WORKDIR /var/www/html/app

RUN usermod -u 1000 www-data

Run chown -R www-data:www-data .

EXPOSE 9000